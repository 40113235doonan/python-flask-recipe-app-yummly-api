from flask import Flask, jsonify,  render_template, request
from random import randint
import requests, json

app = Flask(__name__)

@app.route("/inherits")
def inherits():
    return render_template("base.html")

@app.route("/")
def index():
    return  render_template("index.html")

@app.route("/results")
def results():
    rand = randint(0,5)
    q = request.args.get('q')
    r = requests.get('http://api.yummly.com/v1/api/recipes?_app_id=dd85df7a&_app_key=0b187b4352ca796bc89a4600dfad0f06&requirePictures=true&q= %s' % q)
    data = r.json()
    ID = data["matches"][rand]['id']
    rr = requests.get('http://api.yummly.com/v1/api/recipe/%s?_app_id=dd85df7a&_app_key=0b187b4352ca796bc89a4600dfad0f06' % ID )
    rdata = rr.json()
    return  render_template("results.html", 
    url = rdata['attribution']['url'],
    text = rdata['attribution']['text'],
    logo = rdata['attribution']['logo'],
    name = rdata['name'],
    ttime = rdata['totalTime'],
    i1 = rdata['ingredientLines'][0],
    i2 = rdata['ingredientLines'][1],
    i3 = rdata['ingredientLines'][2],
    i4 = rdata['ingredientLines'][3],
    nrg = rdata['nutritionEstimates'][0]['value'],
    fat = rdata['nutritionEstimates'][1]['value'],
    sfat = rdata['nutritionEstimates'][2]['value'],
    chole = rdata['nutritionEstimates'][3]['value'],
    sug = rdata['nutritionEstimates'][8]['value'],
    pro = rdata['nutritionEstimates'][9]['value'],
    lrgimg = rdata['images'][0]['hostedLargeUrl'],
    rating = data['matches'][rand]['rating'],
    )

@app.route('/error')
def error():
    return render_template('error.html'), 404


if __name__ == "__main__":
   app.run(host='0.0.0.0', debug=True)
