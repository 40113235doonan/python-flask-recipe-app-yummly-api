from flask import Flask, render_template, jsonify, request
import json

app = Flask(__name__)

with open('static/JSON/albums.json') as data_file:
    data = json.load(data_file)


@app.route("/inherits")
def inherits():
    return render_template("base.html")

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/contact")
def contact():
    return  render_template("contact.html")

@app.route("/album")
def album():
    num = request.args.get("num")
    return render_template("album.html",
    print_title = data[str(num)]["Title"],
    print_artist = data[str(num)]["Artist"],
    print_released = data[str(num)]["Released"],
    print_producer = data[str(num)]["Producer"],
    print_genre = data[str(num)]["Genre"],
    print_recordlabel = data[str(num)]["RecordLabel"],
    print_tracks = data[str(num)]["Tracks"],
    print_length = data[str(num)]["Length"],
    print_url  = data[str(num)]["url"]
    )    

if __name__ == "__main__":
    app.run(host='0.0.0.0')
